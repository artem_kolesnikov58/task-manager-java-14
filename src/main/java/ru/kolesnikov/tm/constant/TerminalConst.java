package ru.kolesnikov.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String EXIT = "exit";

    String INFO = "info";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    String TASK_LIST = "task-list";

    String TASK_CLEAR = "task-clear";

    String TASK_CREATE = "task-create";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_VIEW_BY_NAME = "task-view-by-name";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String PROJECT_LIST = "project-list";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_CREATE = "project-create";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String LOGIN = "login";

    String LOGOUT = "logout";

    String REGISTRY = "registry";

    String UPDATE_PASSWORD = "update-password";

    String VIEW_PROFILE = "view-profile";

    String UPDATE_USER_FIRST_NAME = "update-user-first-name";

    String UPDATE_USER_LAST_NAME = "update-user-last-name";

    String UPDATE_USER_MIDDLE_NAME = "update-user-middle-name";

    String UPDATE_USER_EMAIL = "update-user-email";

}
