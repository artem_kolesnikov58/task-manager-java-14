package ru.kolesnikov.tm.exception.empty;

import ru.kolesnikov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}
