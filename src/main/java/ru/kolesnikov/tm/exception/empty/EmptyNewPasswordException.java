package ru.kolesnikov.tm.exception.empty;

import ru.kolesnikov.tm.exception.AbstractException;

public class EmptyNewPasswordException extends AbstractException {

    public EmptyNewPasswordException() {
        super("Error! New password is empty...");
    }

}
