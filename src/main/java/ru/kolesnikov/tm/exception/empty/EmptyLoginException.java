package ru.kolesnikov.tm.exception.empty;

import ru.kolesnikov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
