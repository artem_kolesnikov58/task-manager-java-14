package ru.kolesnikov.tm.exception.empty;

import ru.kolesnikov.tm.exception.AbstractException;

public class EmptyMiddleNameException extends AbstractException {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty...");
    }

}
