package ru.kolesnikov.tm.api.service;

import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password, String email, Role role);

    User updatePassword(String userId, String newPassword);

    User updateUserFirstName(String userId, String newFirstName);

    User updateUserLastName(String userId, String newLastName);

    User updateUserMiddleName(String userId, String newMiddleName);

    User updateUserEmail(String userId, String newEmail);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

}
