package ru.kolesnikov.tm.api.service;

import ru.kolesnikov.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
