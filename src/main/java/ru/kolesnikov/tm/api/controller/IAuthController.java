package ru.kolesnikov.tm.api.controller;

import ru.kolesnikov.tm.entity.User;

public interface IAuthController {

    void login();

    void logout();

    void registry();

    void updatePassword();

    void viewProfile();

    void updateUserFirstName();

    void updateUserLastName();

    void updateUserMiddleName();

    void updateUserEmail();

}
