package ru.kolesnikov.tm.api.repository;

import ru.kolesnikov.tm.dto.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArgs(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
