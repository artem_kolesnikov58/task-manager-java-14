package ru.kolesnikov.tm.service;

import ru.kolesnikov.tm.api.repository.ICommandRepository;
import ru.kolesnikov.tm.api.service.ICommandService;
import ru.kolesnikov.tm.dto.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
