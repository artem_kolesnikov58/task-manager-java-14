package ru.kolesnikov.tm.service;

import ru.kolesnikov.tm.api.service.IAuthService;
import ru.kolesnikov.tm.api.service.IUserService;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.exception.empty.*;
import ru.kolesnikov.tm.exception.user.AccessDeniedException;
import ru.kolesnikov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void updatePassword(String userId, String newPassword) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();
        userService.updatePassword(userId, newPassword);
    }

    @Override
    public void updateUserFirstName(String userId, String newFirstName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        userService.updateUserFirstName(userId, newFirstName);
    }

    @Override
    public void updateUserLastName(String userId, String newLastName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        userService.updateUserLastName(userId, newLastName);
    }

    @Override
    public void updateUserMiddleName(String userId, String newMiddleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        userService.updateUserMiddleName(userId, newMiddleName);
    }

    @Override
    public void updateUserEmail(String userId, String newEmail) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        userService.updateUserEmail(userId, newEmail);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void registry(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        userService.create(login, password);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userService.create(login, password, email);
    }

}
