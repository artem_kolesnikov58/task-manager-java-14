package ru.kolesnikov.tm.controller;

import ru.kolesnikov.tm.api.controller.IAuthController;
import ru.kolesnikov.tm.api.service.IAuthService;
import ru.kolesnikov.tm.api.service.IUserService;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    private final IUserService userService;

    public AuthController(
            final IAuthService authService,
            final IUserService userService
    ) {
        this.authService = authService;
        this.userService = userService;
    }

    @Override
    public void viewProfile() {
        final String userId = authService.getUserId();
        System.out.println("[VIEW USER PROFILE]");
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
        System.out.println("[OK]");
    }

    @Override
    public void updateUserFirstName() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        authService.updateUserFirstName(userId, newFirstName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserLastName() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        authService.updateUserLastName(userId, newLastName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserMiddleName() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME: ");
        final String newMiddleName = TerminalUtil.nextLine();
        authService.updateUserMiddleName(userId, newMiddleName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserEmail() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE USER E-MAIL]");
        System.out.println("ENTER NEW USER E-MAIL: ");
        final String newEmail = TerminalUtil.nextLine();
        authService.updateUserEmail(userId, newEmail);
        System.out.println("[OK]");
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL]");
        final String email = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void updatePassword() {
        System.out.println("[UPDATE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        authService.updatePassword(userId,newPassword);
        System.out.println("[THE PASSWORD WAS SUCCESSFULLY CHANGED]");
        logout();
        System.out.println("[LOG IN TO YOUR ACCOUNT AGAIN]");
    }

}
