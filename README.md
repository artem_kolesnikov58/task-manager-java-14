# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: ARTEM KOLESNIKOV

**E-MAIL**: tema58-rus@yandex.ru

# SOFTWARE

- JDK 1.8
- MAVEN 3.6.3
- MS WINDOWS 10
- INTEL CORE I7-6700HQ 2.60 GHZ

# BUILD THE PROJECT

```bash
mvn clean package
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```